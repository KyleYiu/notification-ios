//
//  ViewController.swift
//  Notification-Prototype
//
//  Created by YiuSaiHo on 23/12/2016.
//  Copyright © 2016 ShoppingGai. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UserNotifications


class ViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    var monitoredRegions: Dictionary<String,NSDate> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set up Location Mannager
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Set up Map View
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        
        setupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == .denied {
            showAlert("Location services were previously denied. Please enable location services for this app in Settings.")
        }
        else if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupData(){
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            
            //Set up region will using related attribute
            let title = "Lorrenzillo's"
            let coordinate = CLLocationCoordinate2DMake(37.703026, -121.759735)
            let regionRadius = 300.0
            
            //Set up region reloadte attribute
            let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,longitude: coordinate.longitude), radius: regionRadius, identifier: title)
            locationManager.startMonitoring(for: region)
            
            //Create annotation
            let restaurantAnnotation = MKPointAnnotation()
            restaurantAnnotation.coordinate = coordinate
            restaurantAnnotation.title = "\(title)"
            mapView.addAnnotation(restaurantAnnotation)
            
            //Draw Circle for mark up the region scope
            let circle = MKCircle(center: coordinate, radius: regionRadius)
            mapView.add(circle)
        }
        else {
            print("System can't track regions")
        }
    }
    
    //Draw Circle
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.strokeColor = UIColor.red
        circleRenderer.lineWidth = 1.0
        return circleRenderer
    }
    
    // When user enter in region
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        showAlert("Eneter \(region.identifier)")
        
        // recording enter time
        monitoredRegions[region.identifier] = NSDate()
    }
    
    //When user exit the region
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        showAlert("Exit \(region.identifier)")
        monitoredRegions.removeValue(forKey: region.identifier)
    }
    
    //Updating region
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        updateRegions()
        //createNotification()
    }
    
    //Checking User stay time
    func updateRegions() {
        let regionMaxVisition = 1.0
        var regionsToDelete: [String] = []
        
        //print("hello world")
        
        for regionsIdentifier in monitoredRegions.keys {
            if NSDate().timeIntervalSince(monitoredRegions[regionsIdentifier]! as Date) > regionMaxVisition{
                showAlert("Thanks for visiting our restaurant")
                print("Thanks for visiting our restaurant")
                createNotification()
                regionsToDelete.append(regionsIdentifier)
            }
        }
        
        for regionIdentifier in regionsToDelete {
            monitoredRegions.removeValue(forKey: regionIdentifier)
        }
    }
    
    //Alert
    func showAlert(_ title:String){
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action: UIAlertAction!) in
            //Do something
        }))
        present(alert,animated: true,completion:nil)
    }
    
    //Create Notification
    func createNotification() {
        let content = UNMutableNotificationContent()
        let imageURL = Bundle.main.url(forResource: "kirin", withExtension: "jpg")
        //系統會自動幫我們生成一組 id。options 參數可做一些進階的設定，有興趣的讀者可進一步查詢 Attachment Attributes 的相關說明。
        let attachment = try! UNNotificationAttachment(identifier: "", url: imageURL!, options: nil)
        content.attachments = [attachment]
        content.title = "體驗過了，才是你的。"
        content.subtitle = "米花兒"
        content.body = "不要追問為什麼，就笨拙地走入未知。感受眼前的怦然與顫抖，聽聽左邊的碎裂和跳動。不管好的壞的，只有體驗過了，才是你的。"
        content.badge = 1
        content.sound = UNNotificationSound.default()
        content.userInfo = ["link":"https://www.facebook.com/himinihana/photos/a.104501733005072.5463.100117360110176/981809495274287"]
        content.categoryIdentifier = "luckyMessage"
        
        //P.S. 
        //UNLocationNotificationTrigger: 使用者靠近某個位置時觸發。比方當你靠近彼得潘家時，手機馬上會收到通知，提醒你找彼得潘喝下午茶。
        //UNTimeIntervalNotificationTrigger: 幾秒鐘後觸發。
        //UNCalendarNotificationTrigger: 指定某個時刻觸發。
        //UNPushNotificationTrigger: 從千里之外的後台傳送到使用者手機的通知，比方彼得潘失戀時緊急發送給大家的討拍拍通知。
        //Remove
        //        open func removePendingNotificationRequests(withIdentifiers identifiers: [String])
        //        open func removeAllPendingNotificationRequests()
        //        open func removeDeliveredNotifications(withIdentifiers identifiers: [String])
        //        open func removeAllDeliveredNotifications()
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification1", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
}

